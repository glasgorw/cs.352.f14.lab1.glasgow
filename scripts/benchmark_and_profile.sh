#!/bin/sh

BENCHMARK_DIR=${GOPATH}/data/benchmarks
IMPLEMENTATIONS="Serial Parallel"

if [ ! -d "${BENCHMARK_DIR}" ]; then
    mkdir -p "${BENCHMARK_DIR}"
fi

cd "${BENCHMARK_DIR}"

go test -c allpairs

for implementation in ${IMPLEMENTATIONS}; do
    echo "Benchmarking and Profiling ${implementation}..."
    ./allpairs.test -test.v -test.run=XXX -test.cpuprofile=${implementation}.CPU.prof -test.bench=AllPairs${implementation}
    ./allpairs.test -test.v -test.run=XXX -test.memprofile=${implementation}.MEM.prof -test.bench=AllPairs${implementation}
done
