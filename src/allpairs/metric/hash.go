package metric
//package main

import (
	"fmt"
	//"time"
//	"image/color"
//	"log"
	"path"
//	"sync"
//
//	"github.com/disintegration/imaging"
//	"github.com/harrydb/go/img/grayscale"
)

const (
	hashWidth  = 8
	hashHeight = 8
	hashPixels = hashWidth * hashHeight
)

// Compute the Perceptual Hash of an Image.
func ComputePerceptualHash(path string) uint64 {
	// Open image for processing

	// Resize image to hashWidth x hashHeight using BSpline

	// Reduce color to grayscale using ToGrayLuminance

	// Compute the average of the color values

	// Compute the hash by setting the bits of a uint64 based on if color
	// value is above or below the mean.

	return 0
}

// Computer Perceptual Hashes for specified files
func ComputePerceptualHashesFromFiles(paths []string) {
	for _, p := range(paths) {
		go func(s string) {
			fmt.Printf("%s %v\n", path.Base(s), ComputePerceptualHash(s));
			//fmt.Printf(p)
		}(p);
	} 
	//fmt.Println("Hello")
}


//func main() {
//	paths := make([]string, 4)
//	paths[0] = "hello/cow.jpg"
//	paths[1] = "hello/pig.jpg"
//	paths[2] = "hello/cat.jpg"
//	paths[3] = "hello/dog.jpg"
//	ComputePerceptualHashesFromFiles(paths)
//	time.Sleep(100);
//}
