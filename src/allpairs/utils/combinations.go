package utils
//package main

// Pair of  values.
type Pair struct {
	First, Second string
}

// Generate combinations of pairs from given set of strings.
func GenerateCombinations(strings []string, all bool) <-chan Pair {
	return nil
}

const DefaultChunkSize = 8 // Default Chunk Size

// Generate chunks of fixed-size from input slice of strings.
func GenerateChunks(strings []string, size int) <-chan []string {
	chunk := make([]string, 0) 
	channel := make(chan []string)
	
	go func() {
		for _, s := range strings {
			chunk = append(chunk, s)
			if len(chunk) % size == 0 {
				channel <- chunk
				chunk = make([]string, 0)
			}
		}
		if len(chunk) > 0 {
			channel <- chunk
		}
		close(channel)

	}()
	
	return channel
}
